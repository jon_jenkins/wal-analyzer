module gitlab.com/jon_jenkins/wal-analyzer

go 1.22.3

require (
	github.com/cespare/xxhash/v2 v2.3.0
	google.golang.org/protobuf v1.34.1
)
