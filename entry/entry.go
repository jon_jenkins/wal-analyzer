package entry

import (
	"encoding/binary"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/cespare/xxhash/v2"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
)

var commitMatcher = regexp.MustCompile(
	`rmgr: Transaction.*?desc: COMMIT (?<timestamp>\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{6} \w{3})`,
)

var lineMatcher = regexp.MustCompile(
	`^rmgr: (?<rmgr>\w+)\s*len\s*\(rec\/tot\):\s*(?<rec_len>\d+)\/\s*(?<tot_len>\d+), .*?desc: (?<desc>\w+).*?(?<blkrefs>blkref.*blk \d+( FPW)?)?$`,
)
var blkrefMatcher = regexp.MustCompile(
	`^blkref #\d+: rel (?<tablespace_oid>\d+)\/(?<database_oid>\d+)\/(?<relfilenode>\d+) (fork \w+ )?blk (?<block>\d+)(?<fpw> FPW)?$`,
)

var transactionMatcher = regexp.MustCompile(
	`rmgr: Transaction.*?desc: (?<desc>\w+).*?$`,
)

const timestampLayout = "2006-01-02 15:04:05.999999 MST"

type ByTotal []Relation

func (l ByTotal) Len() int           { return len(l) }
func (l ByTotal) Less(i, j int) bool { return l[i].Tot > l[j].Tot }
func (l ByTotal) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }

func (e *Entry) IsUnparsed() bool {
	return e.Transaction != nil && e.Transaction.Commit == nil
}

func (e *Entry) IsCommit() bool {
	return e.Transaction != nil && e.Transaction.Commit != nil
}

func (l *Location) Compare(other *Location) bool {
	return l.DatabaseOid == other.DatabaseOid &&
		l.Relfilenode == other.Relfilenode &&
		l.TablespaceOid == other.TablespaceOid
}

func (l *Location) ToString() string {
	return fmt.Sprintf(
		"%d/%d/%d",
		l.TablespaceOid,
		l.DatabaseOid,
		l.Relfilenode,
	)
}

func (l *Location) calculateHash() {
	buf := make([]byte, 3*8)
	binary.LittleEndian.PutUint64(buf[0:8], l.DatabaseOid)
	binary.LittleEndian.PutUint64(buf[8:16], l.Relfilenode)
	binary.LittleEndian.PutUint64(buf[16:24], l.TablespaceOid)
	l.Hash = xxhash.Sum64(buf)
}

func NewLocation(tablespaceOid, databaseOid, relfilenode uint64) *Location {
	l := Location{
		TablespaceOid: tablespaceOid,
		DatabaseOid:   databaseOid,
		Relfilenode:   relfilenode,
	}
	l.calculateHash()
	return &l
}

func ParseCommit(line string) Commit {
	//2023-12-10 00:59:40.401732 UTC
	matches := commitMatcher.FindStringSubmatch(line)
	parsedTime, err := time.Parse(timestampLayout, matches[1])
	if err != nil {
		panic(err)
	}
	return Commit{
		Timestamp: timestamppb.New(parsedTime),
	}

}

func ParseTransaction(line string) *Transaction {
	matches := transactionMatcher.FindStringSubmatch(line)
	var commit Commit
	if matches[1] == "COMMIT" {
		commit = ParseCommit(line)
	}
	return &Transaction{
		Commit: &commit,
	}
}

func ParseEntry(line string) *Entry {
	matches := lineMatcher.FindStringSubmatch(line)
	if matches == nil || len(matches) != 7 {
		return nil
	}
	rmgr := matches[1]
	if rmgr == "Transaction" {
		return &Entry{
			Transaction: ParseTransaction(line),
		}
	}
	return &Entry{
		Rmgr:    matches[1],
		RecLen:  ParseInt(matches[2]),
		TotLen:  ParseInt(matches[3]),
		Desc:    matches[4],
		Blkrefs: ParseBlkrefs(matches[5]),
	}
}

func ParseBlkrefs(blkrefs string) []*Blkref {
	splitBlkRefs := strings.Split(blkrefs, ", ")
	parsedBlkrefs := make([]*Blkref, len(splitBlkRefs))
	for i, v := range strings.Split(blkrefs, ", ") {
		matches := blkrefMatcher.FindStringSubmatch(v)
		if matches == nil || len(matches) != 7 {
			return nil
		}
		fpw := matches[6] == " FPW"
		location := NewLocation(ParseInt(matches[1]), ParseInt(matches[2]), ParseInt(matches[3]))
		parsedBlkrefs[i] =
			&Blkref{
				Location: location,
				Block:    ParseInt(matches[5]),
				Fpw:      fpw,
			}
	}
	return parsedBlkrefs
}

func ParseInt(s string) uint64 {

	i, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		panic(err)
	}
	return i
}
