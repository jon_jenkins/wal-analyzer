package main

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"flag"
	"fmt"
	"io"
	"os"
	"runtime/pprof"
	"sort"

	"gitlab.com/jon_jenkins/wal-analyzer/entry"
	"google.golang.org/protobuf/proto"
)

const blobFileBlockSize = 8192

var inputFileName = flag.String("inputFile", "", "Set input file. If blank, will read from STDIN")
var blobFileName = flag.String("blobFile", "", "Set blob file, will only parse and store. If parseOnly is not set, this will cause the program to read from the file.")
var skipVacuums = flag.Bool("skipVacuums", false, "Use to drop VACUUM operations")
var profile = flag.Bool("profile", false, "Use to enable CPU profiling")
var parseOnly = flag.Bool("parseOnly", false, "Use to parse data and save to a secondary data structure")

func deepCopyMap(original map[uint64]map[string]uint64) map[uint64]map[string]uint64 {
	copy := make(map[uint64]map[string]uint64)

	for key, innerMap := range original {
		newInnerMap := make(map[string]uint64)
		for innerKey, value := range innerMap {
			newInnerMap[innerKey] = value
		}
		copy[key] = newInnerMap
	}

	return copy
}

func analyzeEntries(c chan *entry.Entry) {
	var startCommit *entry.Entry
	var endCommit *entry.Entry

	topN := 10
	writeCounts := make(map[uint64]map[string]uint64)
	writeCountsTilLastCommit := make(map[uint64]map[string]uint64)
	locationMap := make(map[uint64]*entry.Location)
	for entry := range c {
		if entry.IsUnparsed() {
			continue
		}
		if startCommit == nil {
			if entry.IsCommit() {
				startCommit = entry
			}
			continue
		}

		if entry.IsCommit() {
			endCommit = entry
			writeCountsTilLastCommit = writeCounts
			writeCounts = deepCopyMap(writeCountsTilLastCommit)
			continue
		}
		if len(entry.Blkrefs) == 0 {
			continue
		}
		location := entry.Blkrefs[0].Location

		for _, v := range entry.Blkrefs {
			if !location.Compare(v.Location) {
				fmt.Println("Unsupported: blkrefs split across tables:", entry.Blkrefs)
				panic("Exiting")
			}
		}
		// Dump every location into its own map so we can refer back to it
		locationMap[location.Hash] = location
		if _, exists := writeCounts[location.Hash]; !exists {
			writeCounts[location.Hash] = map[string]uint64{"rec": 0, "tot": 0}
		}
		if *skipVacuums && entry.Desc == "VACUUM" {
			continue
		}
		writeCounts[location.Hash]["rec"] += entry.RecLen
		writeCounts[location.Hash]["tot"] += entry.TotLen
	}
	duration := endCommit.Transaction.Commit.Timestamp.AsTime().Sub(startCommit.Transaction.Commit.Timestamp.AsTime()).Seconds()
	writeCountsByRelation := make(map[uint64]map[string]uint64)
	for k, v := range writeCountsTilLastCommit {
		if _, exists := writeCountsByRelation[k]; !exists {
			writeCountsByRelation[k] = map[string]uint64{"rec": 0, "tot": 0}
		}
		writeCountsByRelation[k]["rec"] += v["rec"]
		writeCountsByRelation[k]["tot"] += v["tot"]
	}
	wcSlice := make([]entry.Relation, 0, len(writeCountsByRelation))
	for k, v := range writeCountsByRelation {
		wcSlice = append(wcSlice, entry.Relation{
			Location: k,
			Rec:      v["rec"],
			Tot:      v["tot"],
		})

	}
	sort.Sort(entry.ByTotal(wcSlice))
	for i, v := range wcSlice {
		if i >= topN {
			break
		}
		fmt.Println("Location:", locationMap[v.Location].ToString(), "Rec:", float64(writeCountsByRelation[v.Location]["rec"])/duration, "Tot:", float64(writeCountsByRelation[v.Location]["tot"]))
	}
	fmt.Println("Duration:", duration)

}

func scanLines(inputFile io.ReadCloser, c chan *entry.Entry) {
	scanner := bufio.NewScanner(inputFile)
	readLines := 0
	for scanner.Scan() {
		line := scanner.Text()
		e := entry.ParseEntry(line)
		if e == nil {
			fmt.Printf("Invalid entry: %s\n", line)
			continue
		}
		c <- e
		readLines++
	}
	fmt.Println("Read", readLines, "lines")
	close(c)
}

func readFromBlob(inputFile io.ReadCloser, c chan *entry.Entry) {
	readRecords := uint64(0)
	inputBuf := make([]byte, blobFileBlockSize)
	recordBuf := make([]byte, 2048)
	for {
		n, err := inputFile.Read(inputBuf)
		if err != nil && err != io.EOF {
			panic("Error reading file")
		}
		if n == 0 {
			break
		}
		buf := bytes.NewBuffer(inputBuf)
		var numberOfRecords uint16
		err = binary.Read(buf, binary.LittleEndian, &numberOfRecords)
		if err != nil {
			panic(err)
		}
		for i := uint16(0); i < numberOfRecords; i++ {
			var recordLength uint16
			err = binary.Read(buf, binary.LittleEndian, &recordLength)
			if err != nil {
				panic(err)
			}
			buf.Read(recordBuf[:recordLength])
			e := entry.Entry{}
			err = proto.Unmarshal(recordBuf[:recordLength], &e)
			if err != nil {
				fmt.Println("Record cannot be unmarshaled, skipping")
				continue
			}
			c <- &e
			readRecords += 1
		}
		if err == io.EOF {
			break
		}
	}
	fmt.Println("Read", readRecords, "records from binary blob")
	close(c)

}

func checkEffectiveBufferSize(buf [][]byte) int {
	dataLen := 2
	for _, v := range buf {
		dataLen += len(v) + 2
	}
	return dataLen
}

func writeEntriesToFile(recordsToWrite [][]byte, f io.Writer) uint64 {
	storedEntries := uint64(0)
	blockBuf := bytes.Buffer{}
	blockRecords := len(recordsToWrite)
	binary.Write(&blockBuf, binary.LittleEndian, uint16(blockRecords))
	for _, v := range recordsToWrite {
		recordLen := len(v)
		binary.Write(&blockBuf, binary.LittleEndian, uint16(recordLen))
		blockBuf.Write(v)
		storedEntries += 1
	}

	// Padding
	blockBuf.Write(make([]byte, blobFileBlockSize-blockBuf.Len()))
	if blockBuf.Len() != blobFileBlockSize {
		panic("Incorrect buffer size!")
	}
	_, err := blockBuf.WriteTo(f)
	if err != nil {
		panic(err)
	}
	return storedEntries
}

func storeEntries(fileName string, c chan *entry.Entry) {
	storedEntries := uint64(0)
	f, err := os.Create(fileName)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	preparedForWriting := make([][]byte, 0, 20)

	for entry := range c {
		data, err := proto.Marshal(entry)
		if err != nil {
			fmt.Println("Unable to marshal because", err)
			continue
		}
		if checkEffectiveBufferSize(preparedForWriting)+len(data)+2 >= blobFileBlockSize {
			storedEntries += writeEntriesToFile(preparedForWriting, f)
			preparedForWriting = preparedForWriting[:1]
			preparedForWriting[0] = data
		} else {
			preparedForWriting = append(preparedForWriting, data)
		}
	}
	if len(preparedForWriting) > 0 {
		storedEntries += writeEntriesToFile(preparedForWriting, f)
	}
	fmt.Println("Stored", storedEntries, "entries into blob")

}

func main() {
	flag.Parse()
	entryChan := make(chan *entry.Entry, 10)
	var inputFile io.ReadCloser
	if *inputFileName != "" {
		var err error
		inputFile, err = os.Open(*inputFileName)
		if err != nil {
			panic(err)
		}
		defer inputFile.Close()
	} else {
		inputFile = os.Stdin
	}

	if *profile {
		prof, _ := os.Create("cpu.prof")
		pprof.StartCPUProfile(prof)
		defer prof.Close()
		defer pprof.StopCPUProfile()
	}

	if !*parseOnly {
		if *blobFileName == "" {
			go scanLines(inputFile, entryChan)
		} else {
			blobFile, err := os.Open(*blobFileName)
			if err != nil {
				panic(err)
			}
			defer blobFile.Close()
			go readFromBlob(blobFile, entryChan)
		}
		analyzeEntries(entryChan)
	} else {
		if *blobFileName == "" {
			fmt.Println("You must pass blobFileName to use parseOnly")
			os.Exit(-1)
		}
		go scanLines(inputFile, entryChan)
		storeEntries(*blobFileName, entryChan)
	}
}
